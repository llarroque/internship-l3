\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath, amsfonts, amssymb, amsthm}
\usepackage{stmaryrd}
\usepackage[margin=3cm]{geometry}
\usepackage[hidelinks]{hyperref} %keep it last

\usepackage{todonotes}

% chktex-file 40
% chktex-file 36
% chktex-file 18
% chktex-file 11
% chktex-file 8
% chktex-file 1

\theoremstyle{definition}
\newtheorem{definition}{Definition}[section]

\theoremstyle{definition}
\newtheorem{proposition}{Proposition}[section]

\title{Normalisation Procedures for Existential Rules that Preserve Chase Termination}
\author{Lucas Larroque}
\date{}

\newcommand{\setR}{\ensuremath{\mathcal{R}}}
\newcommand{\spR}{\ensuremath{sp(\setR)}}
\newcommand{\der}{\ensuremath{\mathcal{D}}}
\newcommand{\ens}[1]{\ensuremath{\left\{#1\right\}}}
\newcommand{\refbold}[1]{(\textbf{\ref{#1}})}

\begin{document}

\maketitle
\tableofcontents

\section{Preliminaries}\label{prelim}

This first section introduces every notion we will need later on. We will start with defining the setting in which we work \refbold{gen-def}, and then we will introduce every definition we need for the different chase variants \refbold{chase}. 

\subsection{Setting}\label{gen-def}

We consider a first-order setting limited to \emph{predicates}, \emph{constants}, and \emph{variables}. A \emph{term} is a variable or a constant. Each predicate is associated with an integer, called the \emph{arity} of the predicate.

\subsubsection{Atoms and factbases}

\begin{definition}\label{def-atom}
    An \emph{atom} is of the form $p(e_1, ..., e_n)$ with $p$ a predicate of arity $n$ and $e_1, ..., e_n$ terms.    
    The set of variables, constants and terms appearing in the atom $h$ are respectively denoted with $var(h)$, $cnst(h)$ and $term(h)$.
\end{definition}

\begin{definition}\label{def-fb}
    A \emph{factbase} is an existentially closed conjunction of atoms.
\end{definition}

It is often convenient to see a factbase as a set of atoms. Henceforth, we identify a fact base with the corresponding set of atoms. For instance, \ens{A(x, y), B(x, z, y)} would refer to the factbase $\exists x, y, z . A(x, y)\wedge B(x, z, y)$.\todo{Write a dot between the first occurrence of $z$ and the atom $A(x, y)$.}

Let $A$ be an atom or a factbase. The set of variables, constants and terms appearing in $A$ are respectively denoted with $\textit{var}(A)$, $\textit{cnst}(A)$ and $\textit{term}(A)$.

\subsubsection{Existential rules and knowledge bases}

\begin{definition}\label{def-exist-rule}
    An \emph{existential rule} $R$ is a first-order formula of the following form:
    \[\forall \bar{x} \forall \bar{y},\ B(\bar{x}, \bar{y}) \rightarrow \exists \bar{z},\ H(\bar{x}, \bar{z})\]
  \todo{Explain the notation $H(\bar{x}, \bar{z})$.}  with $\bar{x}$, $\bar{y}$ and $\bar{z}$ sets of variables and $B$ and $H$ conjunctions of atoms, respectively called the \emph{body} and the \emph{head} of the rule.
   \todo{Start the sentence with a word.} $\bar{x}$, which is shared between the body and the head, is called the \emph{frontier} of the rule and is denoted with $fr(R)$.
    $\bar{z}$ is called the \emph{existential variables} of $R$.
\end{definition}
For the sake of simplicity, existential rules will be denoted with $B \rightarrow H$, with all variables appearing in $B$ implicitly universally quantified, and variables in $H$ but not in $B$ existentially quantified.

In the context of this paper, we refer to existential rules as rules.
\begin{definition}\label{def-datalog}
    An existential rule without existential variables is called a \emph{Datalog rule}.
\end{definition}

\begin{definition}\label{def-kb}
    A \emph{knowledge base} is a tuple $\langle R, B \rangle$ where $R$ is a rule set and $B$ is a factbase.
\end{definition}

\subsubsection{Substitutions, homomorphisms and retractions}

\begin{definition}\label{def-subst}
    A \emph{substitution} is a mapping from the set of variables to the set of terms.
\end{definition}
A substitution is often denoted as a set of individual variable mappings.\todo{Clarify what you mean here.}

\begin{definition}\label{def-homo}
    A \emph{homomorphism} from $F$ to $F'$ is a substitution $h:var(F)\to term(F')$ such that $h(F)\subseteq F'$.
\end{definition}
An important property of homomorphisms is that a factbase $F$ logically entails  another fact base $F'$ if and only if there is a homomorphism from $F'$ to $F$.

\begin{definition}\label{def-retr}
    Let $F$ and $F'$ be two sets of atoms such that $F'\subset F$. A homomorphism $\sigma$ is a \emph{retraction} from $F$ to $F'$ if $\sigma_{|F'} = id_{F'}$.
    If a retraction exists from $F$ to $F'$, $F'$ is called a \emph{retract} of $F$.
\end{definition}

\subsection{The chase algorithm}\label{chase}

The chase is a family of algorithms with the goal of computing a universal model for a knowledge base.\todo{Define universal model and the semantics of our fragment.} To define it, we will first need a few notions.

\subsubsection{Triggers and derivations}

\begin{definition}\label{def-trig}
    Let $F$ be a factbase and $R = B\to H$ an existential rule. If there is a homomorphism $\pi$ from $B$ to $F$, then we say that $R$ is applicable on $F$ via $\pi$. In this case, the pair $t = (R, \pi)$\todo{Shouldn't this include $F$.} is called a \emph{trigger}.

    We denote $\pi(B)$ by $support(t)$.
\end{definition}
When applying a trigger $t$, we extend $\pi$ to map every existential variable with a fresh variable indexed by $t$\todo{Be a bit more precise about how these new variables are instantiated}. Such extension is denoted by $\hat\pi$.

\begin{definition}\label{def-immder}
    Let $F$ be a factbase and $t = (R, \pi)$ a trigger, with $R = B\to H$. The factbase $F\cup\hat\pi(H)$, which is the result of applyting $t$ to $F$, is called an \emph{immediate derivation} from $F$ through $t$.

    We denote $\hat\pi(H)$ by $output(t)$.
\end{definition}

\begin{definition}\label{def-der}
    Let $\mathcal{K} = (F, \setR)$ be a knowledge base. A \emph{derivation} from $\mathcal{K}$ is a possibly infinite sequence $\der = (\emptyset, F_0), (t_1, F_1), (t_2, F_2), ...$ where $F_0 = F$, and for each $i>0$, $t_i$ is a trigger, $F_{i}$ is a factbase, and $F_{i}$ is an immediate derivation from $F_{i-1}$ through $t_i$, and for each $i \neq j$, $t_i \neq t_j$.

    The set of all triggers appearing in \der\ is denoted by $triggers(\der)$. The result of the application of the derivation \der\ on $F$ is $F^{\der} = \bigcup_i F_i$
\end{definition}
The $k$-prefix of a derivation \der\ is the result of the application of the first $k$ triggers of \der, and is denoted by $\der_{|k}$. We can also say that \der\ is an extension of $\der_{|k}$.

\subsubsection{Chase variants}

The chase variants differ from each other in which triggers are applicable, as highlighted in the next definition.

\begin{definition}\label{def-xapp}
    Let $\mathcal{K} = (F, \setR)$ be a knowledge base, \der\ be a derivation from $\mathcal{K}$, and $t = (R, \pi)$ a trigger. Let us assume $R$ is applicable on $F^\der$. Then $t$ is:
    \begin{itemize}
        \item \emph{\textbf{O}-applicable} on \der\ if $t\notin triggers(\der)$
        \item \emph{\textbf{SO}-applicable} on \der\ if there is no trigger $(R, \pi')\in triggers(\der)$ such that $\pi_{|fr(R)} = \pi'_{|fr(R)}$
        \item \emph{\textbf{R}-applicable} on \der\ if there is no retraction from $F^\der\cup output(t)$ to $F^\der$
        \item \emph{\textbf{E}-applicable} on \der\ if there is no homomorphism from $F^\der\cup output(t)$ to $F^\der$
    \end{itemize}
\todo{I am a bit confused by this definition; let's discuss.}
\end{definition}

\begin{definition}\label{def-chase}
    Let $\textbf{X} \in \ens{\textbf{O}, \textbf{SO}, \textbf{R}, \textbf{E}}$. A derivation \der\ for which every trigger $t_i\in triggers(\der)$ is \textbf{X}-applicable on $\der_{|i-1}$ is called an \emph{\textbf{X}-derivation}. The class of all \textbf{X}-derivation is called the \emph{\textbf{X}-chase}.
\end{definition}

\subsubsection{Termination}

The notion of termination is reliant on the notion of fairness.

\begin{definition}\label{def-fair}
    An \textbf{X}-derivation \der\ is \emph{fair} if whenever a trigger $t$ is \textbf{X}-applicable on $\der_{|i}$, there exists a $k > i$ such that either $t_k = t$ or $t$ is not \textbf{X}-applicable on $\der_{|k}$.\todo{The first condition is this disjunction is not necessary.}
\end{definition}

\begin{definition}\label{def-term}
    An \textbf{X}-derivation is \emph{terminating} if it is both fair and finite.

    We say that the \textbf{X}-chase terminates on a knowledge base $\mathcal{K}$ if every fair \textbf{X}-derivation from $\mathcal{K}$ is finite.
    
\todo{Discuss sometimes/always termination and how this affects the restricted chase.}
\end{definition}

\section{Equivalent translations}\label{equiv-trans}

In this section we will first talk about an equivalent translation, the single-piece translation \refbold{sp-trans}, which preserves logical equivalence \refbold{sp-eq}. We will then see how the termination of the different chase variants are affected by this translation \refbold{sp-ter}.

\subsection{Single-piece translation}\label{sp-trans}

\begin{definition}\label{def-pieceg}
    Let $R = B \rightarrow H$. The \emph{piece graph} of $R$ is the graph which vertices are the atoms appearing in $H$, and with the following edge relation:
    \[h_1 \text{ is in relation with } h_2 \Leftrightarrow \exists z \text{ existential variable } .\  z\in var(h_1) \wedge z\in var(h_2)\]
\end{definition}
\todo{Pieces are ``glued together'' with existential variables!}

\begin{definition}\label{def-rpiece}
    Let $R = B \rightarrow H$. A \emph{rule piece} of $R$ is the conjunction of atoms corresponding to a connected component of its piece graph.
\end{definition}

\begin{definition}\label{def-sp-trans}
    Let \setR\ be a set of existential rules. The \emph{single-piece translation} of \setR, denoted with \spR, is defind as follow. For each rule $R = B \rightarrow H$, for each rule piece $H_i$, add in \spR\ the rule $B \rightarrow H_i$.
\end{definition}
\todo{Merge the three previous definitions; also define piece transformation on a rule and then on a rule set.}

\subsection{Equivalence of the single-piece translation}\label{sp-eq}

\begin{proposition}\label{prop-sp-eq}
    A rule set \setR{} is equivalent to the set \spR.
\end{proposition}

\begin{proof}
    Let $R = B \rightarrow H$ be a rule in \setR, $H_1, ..., H_m$ its pieces and $R'_1, ..., R'_m$ the rules associated in \spR. Existential variables appearing in a piece are disjoint from those appearing in another piece (if they were not they would not be in different connected components in the piece graph of $R$). Thus, we can rewrite $R$ with the following form:
    \[R = \forall \bar{x} \forall \bar{y},\ B(\bar{x},\bar{y}) \rightarrow (\exists \bar{z}_1,\ H_1(\bar{x}, \bar{z}_1))\wedge ... \wedge (\exists \bar{z}_m,\ H_m(\bar{x}, \bar{z}_m))\]

    $\forall A,\ \forall B,\ \forall C,\ ((A \rightarrow B) \wedge (A \rightarrow C)) \leftrightarrow (A \rightarrow B\wedge C)$, thus $R'_1\wedge ...\wedge R'_m \leftrightarrow R$. Every rule in \spR\ being extracted from a rule in \setR, we have the equivalence.
\todo{Improve structure: deal with both directions of the proposition separately. That is, first show that a model of \setR{} is also a model \spR. Then the other direction.}
\end{proof}

\subsection{Preservation of chase termination}\label{sp-ter}

\begin{proposition}\label{prop-sp-ter}
    Single-piece translation preserves the termination of oblivious, semi-oblivious, restricted and equivalent chase.
\end{proposition}

\begin{proof}
    Let $(F,\ \setR)$ be a knowledge base on which the \textbf{X}-chase terminates.

    \noindent Let $\der^{sp} = (\emptyset,\ F),(t^{sp}_1, F_1),...$ a fair \textbf{X}-derivation from $(F,\ \spR)$.
    \vspace{0.2cm}
    
    \noindent For each $n$, let us define $t_n$ as the trigger $(R_n, \pi_n)$ where $t^{sp}_n = (R^{sp}_n, \pi_n)$ and $R^{sp}_n$ is a single-piece rule derived from $R_n$. Thus, $R_n$ and $R^{sp}_n$ share the same body, and if $t^{sp}_n$ is \textbf{X}-applicable upon $F_n$, then $t_n$ is too.
    
    Let \der\ be the derivation which starts with $(\emptyset,\ F)$, and on which we apply each \textbf{X}-applicable $t_n$ one after the other. One can imagine \der\ as $\der^{sp}$ in which every $t^{sp}_n$ is replaced by $t_n$ and unapplicable triggers are removed.
    
    \der\ is fair, because if it was not the case, there would exist a trigger \textbf{X}-applicable on $F^\der$, and thus a trigger \textbf{X}-applicable on $F^{\der^{sp}}$, which would contradict $\der^{sp}$'s fairness.
    \vspace{0.2cm}
    
    \noindent The \textbf{X}-chase terminates on $(F,\ \setR)$, thus \der\ is finite. Let $N$ be its length.
    \vspace{0.2cm}
    
    \noindent \textbf{(Proof idea - WIP)} If a trigger $t_n$ is not \textbf{X}-applicable on $F_n$, it is because the same trigger has already been applied before. This can happen only $m$ times per rule in \setR, with $m$ the max number of rules formed in \spR from a single rule of \setR. Thus, $\der^{sp}$ cannot be longer than $N\times m$, which is finite.
\end{proof}

Comment regarding the restricted chase: consider the rule set $\setR = R(x, y) \to \exists z, w . R(y, z) \wedge R(y, w) \wedge R(w, y)$, which does not terminate w.r.t. the restricted chase. Note that the rule set \spR does sometimes terminate with respect to the restricted chase.

Note that the situation described above for the restricted chase does not happen (I reckon) for the other chase variants.

\section{Normalizing procedures}\label{normal-proc}

In this section we will discuss the preservation of chase termination through a normalizing procedure that ensures that every rule will only have one atom in its head. \todo{Before moving on, describe the notion of a conservative extension.}

\subsection{Single atom-headed normalization}\label{normal-def}

\begin{definition}\label{def-norm}
    Let $R = B_1\wedge...\wedge B_n \to H_1 \wedge ... \wedge H_o$ be a rule, with $B_i$ and $H_j$ atoms for every $i$ and $j$. Let us denote the variables appearing in the head of $R$ by $y_1$,...,$y_p$.
    
    \noindent The normalization procedure applied to $R$ outputs the following rules:
    \begin{itemize}
        \item $B_1 \wedge ... \wedge B_n \to X_R(y_1, ..., y_p)$
        \item for each $j\leq o$, $X_R(y_1, ..., y_p) \to H_j$ 
        \item $H_1 \wedge ... \wedge H_o \to X_R(y_1,..., y_p)$
    \end{itemize}
    where $X_R$ is a fresh predicate of arity $p$.
\end{definition}
\end{document}
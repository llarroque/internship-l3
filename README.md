# Internship L3

Contains LaTeX and pdf files related to my internship at INRIA Montpellier.

Subject: Normalisation Procedures for Existential Rules that Preserve Chase Termination